import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import Game from './game/game'

// ========================================

ReactDOM.render(
<Game />,
document.getElementById('root')
);
